package main

import (
	"bitbucket.org/jszydlowski/nesgo/rom"
	"fmt"
)

func main() {
	defer fmt.Println("[DEFERED END]")
	defer fmt.Println("[DEFERED END2]")

	fmt.Println("ngo version 0.000")
	fmt.Println("[START]")

	rom.Info()

	fmt.Println("[END]")
}
